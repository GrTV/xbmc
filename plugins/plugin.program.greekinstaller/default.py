import os
import sys
import urllib
import xbmc
import xbmcaddon
import xbmcgui
import xbmcplugin

import downloader
import extract

ADDON = xbmcaddon.Addon(id='plugin.program.greekinstaller')


def CATEGORIES():
    dialog = xbmcgui.Dialog()
    dp = xbmcgui.DialogProgress()
    dp.create("Greek Installer", "Downloading Code ", '', 'Please Wait')
    keyword = SEARCH()
    if not keyword:
        return
    url = 'http://bit.ly/greekinstaller' + keyword
    path = xbmc.translatePath(os.path.join('special://home/addons', 'packages'))
    lib = os.path.join(path, keyword + '.zip')
    addonfolder = xbmc.translatePath(os.path.join('special://home', ''))

    downloader.download(url, lib)

    dp.update(0, "", "Extracting and installing...")
    extract.all(lib, addonfolder, dp)
    xbmc.executebuiltin('UpdateLocalAddons')
    xbmc.sleep(500)
    if int(xbmcaddon.Addon('xbmc.python').getAddonInfo('version').replace('.', '')) >= 2250 and keyword == '0':
        xbmc.executeJSONRPC('{"jsonrpc":"2.0","method":"Addons.SetAddonEnabled","params":{"addonid":"repository.grtv.xbmc-addons","enabled":true},"id":1}')
        xbmc.executebuiltin('UpdateAddonRepos')
        if not xbmc.getCondVisibility('System.HasAddon(script.grtv.stb)'):
            xbmc.sleep(2000)
            xbmc.executebuiltin('InstallAddon(script.grtv.stb)')
    else:
        xbmc.executebuiltin('Dialog.Close(busydialog,true)')
        xbmc.executebuiltin('UpdateAddonRepos')

    dialog.ok("Greek Installer", "Installation Complete", "", "[COLOR red]Public code \"{0}\" installed[/COLOR]".format(keyword))
    if not xbmc.getCondVisibility('System.HasAddon(script.grtv.stb)') and int(xbmcaddon.Addon('xbmc.python').getAddonInfo('version').replace('.', '')) <= 2240 and keyword == '0':
        xbmc.sleep(500)
        xbmc.executebuiltin('RunPlugin("plugin://script.grtv.stb/")')


def SEARCH():
    search_entered = ''
    keyboard = xbmc.Keyboard(search_entered, 'Enter install code')
    keyboard.doModal()
    if keyboard.isConfirmed():
        search_entered = keyboard.getText().replace(' ', '%20')
        if not search_entered:
            return
    return search_entered


def get_params():
    param = []
    paramstring = sys.argv[2]
    if len(paramstring) >= 2:
        params = sys.argv[2]
        cleanedparams = params.replace('?', '')
        if params[len(params) - 1] == '/':
            params = params[0:len(params) - 2]
        pairsofparams = cleanedparams.split('&')
        param = {}
        for i in range(len(pairsofparams)):
            splitparams = {}
            splitparams = pairsofparams[i].split('=')
            if (len(splitparams)) == 2:
                param[splitparams[0]] = splitparams[1]

    return param


params = get_params()
url = None
name = None
mode = None
iconimage = None
description = None

try:
    url = urllib.unquote_plus(params["url"])
except:
    pass
try:
    name = urllib.unquote_plus(params["name"])
except:
    pass
try:
    iconimage = urllib.unquote_plus(params["iconimage"])
except:
    pass
try:
    mode = int(params["mode"])
except:
    pass

# these are the modes which tells the plugin where to go
if mode is None or url is None or len(url) < 1:
    CATEGORIES()

# xbmcplugin.endOfDirectory(int(sys.argv[1]))
