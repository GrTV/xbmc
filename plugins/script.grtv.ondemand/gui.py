import datetime
import threading
import time

import xbmc
import xbmcgui
import xbmcaddon
#import source as src
#from notification import Notification
#from strings import *
#import buggalo

#import streaming

DEBUG = False

ACTION_LEFT = 1
ACTION_RIGHT = 2
ACTION_UP = 3
ACTION_DOWN = 4
ACTION_PAGE_UP = 5
ACTION_PAGE_DOWN = 6
ACTION_SELECT_ITEM = 7
ACTION_PARENT_DIR = 9
ACTION_PREVIOUS_MENU = 10
ACTION_PREVIOUS_BACK = 107
ACTION_SHOW_INFO = 11
ACTION_NEXT_ITEM = 14
ACTION_PREV_ITEM = 15

KEY_NAV_BACK = 92
KEY_CONTEXT_MENU = 117
KEY_HOME = 159

CANADA_ICON = 4601

HALF_HOUR = datetime.timedelta(minutes = 30)

ADDON = xbmcaddon.Addon(id = 'script.grtv.ondemand')

def debug(s):
    if DEBUG: xbmc.log(str(s), xbmc.LOGDEBUG)

class Point(object):
    def __init__(self):
        self.x = self.y = 0

    def __repr__(self):
        return 'Point(x=%d, y=%d)' % (self.x, self.y)

class OnDemandGuide(xbmcgui.WindowXML):


    def __new__(cls):
        return super(OnDemandGuide, cls).__new__(cls, 'script-ondemand-main.xml', ADDON.getAddonInfo('path'))

    def __init__(self):
        super(OnDemandGuide, self).__init__()
        #self.setFocusId(CANADA_ICON)

    def setFocusId(self, controlId):
        control = self.getControl(controlId)
        if control:
            self.setFocus(control)

    def setFocus(self, control):
        debug('setFocus %d' % control.getId())
        super(OnDemandGuide, self).setFocus(control)

    def onAction(self, action):
        if action.getId() == ACTION_PREVIOUS_MENU:
            print 'got here ACTION_PREVIOUS_MENU'
            self.close()
        elif action.getId() == KEY_NAV_BACK:
            print 'got here KEY_NAV_BACK'
            self.close()

    def onControl(self, control):
        print 'button pressed %d' % control.getId()
        if control.getId() == CANADA_ICON:
            print 'CANADA_ICON button pressed %d' % control.getId()

    def onClick(self, controlId):
        print 'ControlID: %s' % controlId
        if controlId == CANADA_ICON:
            print 'CANADA_ICON button pressed'

#        if self.isClosing:
#            return
