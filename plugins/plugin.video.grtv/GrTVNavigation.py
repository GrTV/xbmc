import os
import sys
import urllib
import xbmc
import xbmcgui
import xbmcaddon
from uuid import uuid4

language            = xbmcaddon.Addon().getLocalizedString

class Navigation():
    def __init__(self):
        self.xbmc = sys.modules["__main__"].xbmc
        self.xbmcgui = sys.modules["__main__"].xbmcgui
        self.xbmcplugin = sys.modules["__main__"].xbmcplugin

        self.settings = sys.modules["__main__"].settings
        self.service = sys.modules["__main__"].service
        self.settingsItem = {'Title':'My GRTV'  , 'Logo':self.service.getPluginImage('settings'), 'action':"settings" }
        self.HdItem = {'Title':'HD'  , 'Logo':self.service.getPluginImage('hd'), 'action':"hd" }

    def showSettings(self, check = False):
        show = False
        
        if(check):
            if (not self.settings.getSetting("firstrun")):
                show = True
                self.settings.setSetting("firstrun", "1")
        
            if (self.service.userName() == ""):
                show = True
                
            if (self.service.userPassword() == ""):
                show = True
        
            if (not self.settings.getSetting("appId")):
                show = True
                self.settings.setSetting("appId", str(uuid4()))
#        old_user_name = self.service.userName()
#        old_user_password = self.service.userPassword()
        else:
            show = True
            
        if(show):
            self.settings.openSettings()
    
            user_name = self.service.userName()
            user_password = self.service.userPassword()
    
            result = ""
            status = 500
    
            if not user_name:
                return (result, 200)
    
            self.xbmc.executebuiltin("Container.Refresh")
            return (result, status)

    def MainMenu(self):
        self.CategoryMenu()
        self.addListItem(self.settingsItem)
        self.xbmcplugin.endOfDirectory(handle=int(sys.argv[1]), succeeded=True, cacheToDisc=False)

    def subMenu(self, params={}):
        get = params.get

        if (not get("action")):
            self.addListItem(self.settingsItem)    
        else:
            if (get("action") == 'category'):
                self.ChannelMenu(get("id"))
                self.xbmcplugin.endOfDirectory(handle=int(sys.argv[1]), succeeded=True, cacheToDisc=False)
            elif (get("action") == 'channel'):
                self.StartPlayer(params)
#                self.xbmcplugin.endOfDirectory(handle=int(sys.argv[1]), succeeded=True, cacheToDisc=False)
            elif (get("action") == 'settings'):
                self.showSettings()
            elif (get("action") == 'hd'):
                line1 = language(30850).encode("utf-8")
                line2 = language(30851).encode("utf-8")
                xbmcgui.Dialog().ok("Information", line1, line2)

                xbmc.executebuiltin('RunPlugin(plugin://plugin.video.hellenic.tv/?action=dialog)')
            else:
                print plugin + " ARGV Nothing done.. verify params " + repr(params)

    def CategoryMenu(self):
        if(not self.service.userName() == "" and not self.service.userPassword() == "" ):
            categories = self.service.getCategories()
            if(categories):
                for cat in categories:
                    self.addListItem(cat)
            self.addListItem(self.HdItem)    

    def ChannelMenu(self, categoryID = ''):
        if(not self.service.userName() == "" and not self.service.userPassword() == "" ):
            channels = self.service.getChannels(categoryID)

            if(channels):
                for ch in channels:
                    self.addListItem(ch)

    def StartPlayer(self, params={}):
        get = params.get

        icon = self.service.getImageByChannel(get('id'))
        thumbnail = icon

        listitem = self.xbmcgui.ListItem(get("Title"), iconImage=icon, thumbnailImage=thumbnail)

        url = self.service.getMediaUrl(get('id'))

        listitem.setProperty("Video", "true")
        listitem.setInfo(type='Video', infoLabels=params)
        
        xbmc.Player().play(item = url, listitem = listitem)

    def addListItem(self, params={}):
        item = params.get
        folder = True
        
        icon = item("Logo", self.service.getPluginImage('explore'))
        thumbnail = item("Logo", self.service.getPluginImage('explore'))
        listitem = self.xbmcgui.ListItem(item("Title").encode("utf-8"), iconImage=icon, thumbnailImage=thumbnail)

        url = '%s?path=%s&' % (sys.argv[0], item("path"))
        url += 'action=' + item("action") + '&'
        url += 'Title=' + item("Title").encode("utf-8")
        if (item("action") == "category"):
            url += '&id=' + item("CategoryID")
        if (item("action") == "channel"):
            url += '&id=' + item("ChannelID").encode("utf-8")
        if (item("Logo")):
            url += '&Logo=' + item("Logo").encode("utf-8")
        self.xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]), url=url, listitem=listitem, isFolder=folder)
 