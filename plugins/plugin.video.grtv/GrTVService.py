import os
import sys
from uuid import uuid4
import urllib
import urllib2
import httplib
from xml.dom.minidom import parseString

class Service:
    HOST = 'admin.grtv.mobi'
    basepath = '/DeskTopModules/DnnIptv/Services/RestPlayerService.svc/'
    imagepath = '/DeskTopModules/DnnIptv/Services/DbImage.aspx?ch='
    rest = {}
    rest['categories'] = 'channel/category'
    rest['channels'] = "channel/category/%s"
    rest['play'] = "channel/%s/play/False"

    def __init__(self):
        self.xbmc = sys.modules["__main__"].xbmc
        self.settings = sys.modules["__main__"].settings
        self.INVALID_CHARS = "\\/:*?\"<>|"
        self.ICONS_PATH = os.path.join(self.settings.getAddonInfo('path'), "Icons")

    def _getUserCredentials(self):
        xml = '<PlayerLogin xmlns="http://schemas.datacontract.org/2004/07/DnnIptv.PlayerService.Common.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ApplicationID>' + self.applicationID() + '</ApplicationID><ApplicationName>XBMC Plugin</ApplicationName><ApplicationVersion>1.0.0</ApplicationVersion><DeviceType>XBMC</DeviceType><Login>' + self.userName() + '</Login><Password>' + self.userPassword() + '</Password></PlayerLogin>'
        return xml
        
    def getImageByChannel(self, channelID = ''):
        url = 'http://' + self.HOST + self.imagepath + channelID
        return url
        
    def _postRequest (self, url ):
        retval = ''

        try: 
            conn = httplib.HTTPSConnection(host = self.HOST)
            xml = self._getUserCredentials()
            headers = {
                "Content-type": "text/xml",
                "Content-Length": len(xml)
                }
            conn.request("POST", url, headers = headers)
            conn.send(xml)
            response = conn.getresponse()
            if(response.status == 200):
                retval = response.read()
            else:
                print('HTTP Error: %s' % response.status)
        except Exception, ex:
            print ('[_postRequest] Uncaught exception in GrTVService.py: %s' % str(ex)) 
            
        finally:
            conn.close()
        
        return retval
    
    def getPluginImage(self, title):
        if (not title):
            title = "DefaultFolder"
        thumbnail = os.path.join(self.ICONS_PATH, title + ".png")
        if (not os.path.isfile(thumbnail)):
            thumbnail = "DefaultFolder.png"
        return thumbnail

    def userName(self):
        return self.settings.getSetting("username")

    def userPassword(self):
        return self.settings.getSetting("password")

    def applicationID(self):
        return self.settings.getSetting("appId")

    def getCategories(self):
        url = self.basepath + self.rest['categories']
        xml = self._postRequest (url = url)
        categories = []
        if(not xml == ""):
            doc = parseString(xml)
                 
            for node in doc.getElementsByTagName("ClientCategoryInfo"):
                categories.append({
                            'CategoryID': self.getXmlValue (node, 'CategoryID'),
                            'Title': self.getXmlValue (node, 'Category'),
                            'action':'category',
                            'folder':'true'
                        })
        return categories

    def getChannels(self, categoryID):
        url = self.basepath + self.rest['channels'] % categoryID
        xml = self._postRequest (url = url)
        
        channels = []

        if(not xml == ""):
            doc = parseString(xml)
                 
            for node in doc.getElementsByTagName("ClientChannelInfoExtended"):
                channels.append({
                            'ChannelID': self.getXmlValue (node, 'ChannelID'),
                            'SortOrder': self.getXmlValue (node, 'SortOrder'),
                            'Title': self.getXmlValue (node, 'Title'),
                            'Description': self.getXmlValue (node, 'Description'),
                            'Logo': self.getXmlValue (node, 'ImageUrl'),
                            'action':'channel',
                            'folder':'true'
                        })
        return channels

    def getMediaUrl(self, channelID = ''):
        retval = ''
        url = self.basepath + self.rest['play'] % channelID
        xml = self._postRequest (url)

        if(not xml == ""):
            doc = parseString(xml)
            urlxml = doc.documentElement.firstChild
            retval = 'http://' + urlxml.nodeValue
        return retval

    def getXmlValue (self, root, tag):
        retval = ''
        for child in root.childNodes:
            if(child.tagName == tag):
                try:
                    tagvalue = child.firstChild
                    retval =  tagvalue.nodeValue
                except:
                    retval = ""
        return retval



