import sys
import xbmc
import xbmcgui
import xbmcaddon
import xbmcplugin
import CommonFunctions as common


# xbmc hooks
settings = xbmcaddon.Addon(id='plugin.video.mygrtv')


if (__name__ == "__main__" ):
    import GrTVService
    service = GrTVService.Service()
    import GrTVNavigation
    navigation = GrTVNavigation.Navigation()
    
    try:
        navigation.showSettings(check = True)
        
        if (not sys.argv[2]):
            navigation.MainMenu()
        else:
            params = common.getParameters(sys.argv[2])
            navigation.subMenu(params)
    except Exception, ex:
        print ('[plugin.video.grtv] Uncaught exception in addon.py: %s' % str(ex) , xbmc.LOGDEBUG)   
