#
#      Copyright (C) 2013 Team GrTV
#      http://www.grtv.mobi
#
#  This Program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2, or (at your option)
#  any later version.
#
#  This Program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this Program; see the file LICENSE.txt.  If not, write to
#  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#  http://www.gnu.org/copyleft/gpl.html
#

import sys
import xbmc
import xbmcgui
import xbmcplugin
import grtvSettings
import gui
import home

# xbmc hooks
try:
    settings = grtvSettings.grtvSettings()
    try:
        print "Updating Home Menu on Guide Startup"
        updater = home.homeUpdater(settings)
        updater.OverwriteHome()
        
    except Exception, ex:
        print ('<<<<<<<<<<<< ERROR')
        print ('[script.grtv.stb] Uncaugt exception in addon.py while updating home screen: %s' % str(ex) , xbmc.LOGDEBUG)
        print ('ERROR >>>>>>>>>>>>')
        
    if (settings.userName() == '' or settings.userPassword() == ''):    
        xbmc.executebuiltin("RunPlugin(plugin://plugin.video.mygrtv?action=settings)")
    else:
        if(settings.ShowContextHelp()):
            line1 = "TO REFRESH THE GUIDE"
            line2 = "PRESS THE \"CONTEXT BUTTON\""
            line3 = "THE ONE <WITH THE 3 LINES>"
            xbmcgui.Dialog().ok("GUIDE TIP", line1, line2, line3)

        w = gui.TVGuide()
        w.doModal()
        w.close()
        del w

except Exception, ex:
    print ('<<<<<<<<<<<< ERROR')
    print ('[script.grtv.stb] Uncaught exception in addon.py: %s' % str(ex) , xbmc.LOGDEBUG)
    print ('ERROR >>>>>>>>>>>>')

