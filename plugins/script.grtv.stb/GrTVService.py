import os
import sys
from uuid import uuid4
import urllib
import urllib2
import httplib
import grtvSettings
from xml.dom.minidom import parseString

class Service:
    HOST = 'admin.grtv.mobi'
    basepath = '/DeskTopModules/DnnIptv/Services/RestPlayerService.svc/'
    imagepath = '/DeskTopModules/DnnIptv/Services/DbImage.aspx?ch='
    rest = {}
    rest['categories'] = 'channel/category'
    rest['channels'] = "channel/category/%s"
    rest['programs'] = "channel/%s/program"
    rest['guide'] = "channel/%s/program/%s" 
    rest['guideall'] = "channel/category/%s/program/%s" 
    rest['play'] = "channel/%s/play/False"

    def __init__(self):
        self.xbmc = sys.modules["__main__"].xbmc
        self.settings = grtvSettings.grtvSettings()
        self.INVALID_CHARS = "\\/:*?\"<>|"
        self.ICONS_PATH = os.path.join(self.settings.path(), "Icons")

    def _getUserCredentials(self):
        xml = '<PlayerLogin xmlns="http://schemas.datacontract.org/2004/07/DnnIptv.PlayerService.Common.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ApplicationID>' + self.settings.applicationID() + '</ApplicationID><ApplicationName>XBMC Plugin</ApplicationName><ApplicationVersion>1.0.0</ApplicationVersion><DeviceType>XBMC</DeviceType><Login>' + self.settings.userName() + '</Login><Password>' + self.settings.userPassword() + '</Password></PlayerLogin>'
        return xml
        
    def getImageByChannel(self, channelID = ''):
        url = 'http://' + self.HOST + self.imagepath + channelID
        return url
        
    def _postRequest (self, url ):
        retval = ''

        try: 
            conn = httplib.HTTPSConnection(host = self.HOST)
            xml = self._getUserCredentials()
            headers = {
                "Content-type": "text/xml",
                "Content-Length": len(xml)
                }
            conn.request("POST", url, headers = headers)
            conn.send(xml)
            response = conn.getresponse()
            if(response.status == 200):
                retval = response.read()
            else:
                print('HTTP Error: %s' % response.status)
        except Exception, ex:
            print ('[_postRequest] Uncaught exception in GrTVService.py: %s' % str(ex)) 
            
        finally:
            conn.close()
        
        return retval
    
    def getPluginImage(self, title):
        if (not title):
            title = "DefaultFolder"
        thumbnail = os.path.join(self.ICONS_PATH, title + ".png")
        if (not os.path.isfile(thumbnail)):
            thumbnail = "DefaultFolder.png"
        return thumbnail

    def getCategories(self):
        url = self.basepath + self.rest['categories']
        xml = self._postRequest (url = url)
        categories = []
        if(not xml == ""):
            doc = parseString(xml)
                 
            for node in doc.getElementsByTagName("ClientCategoryInfo"):
                categories.append({
                            'CategoryID': self.getXmlValue (node, 'CategoryID'),
                            'Title': self.getXmlValue (node, 'Category'),
                            'action':'category',
                            'folder':'true'
                        })
        return categories

    def getChannelsAndPrograms(self, categoryID, date):
        url = self.basepath + self.rest['guideall'] % (categoryID, date.strftime('%Y-%m-%d'))
        xml = self._postRequest (url = url)
        
        channels = []
        programs = []

        if(not xml == ""):
            print("XML: " + xml)
            doc = parseString(xml)
                 
            for node in doc.getElementsByTagName("ClientChannelInfoExtended"):
                channels.append({
                            'ChannelID': self.getXmlValue (node, 'ChannelID'),
                            'SortOrder': self.getXmlValue (node, 'SortOrder'),
                            'Title': self.getXmlValue (node, 'Title'),
                            'Description': self.getXmlValue (node, 'Description'),
                            'Logo': self.getXmlValue (node, 'ImageUrl'),
                            'action':'channel',
                            'folder':'true'
                        })

            for node in doc.getElementsByTagName("a:ProgramInfo"):
                programs.append({
                            'ChannelID': self.getXmlValue (node, 'a:ChannelID'),
                            'Description': self.getXmlValue (node, 'a:Description'),
                            'Duration': self.getXmlValue (node, 'a:Duration'),
                            'EndTime': self.getXmlValue (node, 'a:EndTime'),
                            'ProgramID': self.getXmlValue (node, 'a:ProgramID'),
                            'RunTime': self.getXmlValue (node, 'a:RunTime'),
                            'StartTime': self.getXmlValue (node, 'a:StartTime'),
                            'Title': self.getXmlValue (node, 'a:Title'),
#                            'Logo': self.getXmlValue (node, 'ImageUrl'),
                            'action':'program',
                            'folder':'true'
                        })
                
        return channels, programs

    def getChannels(self, categoryID):
        url = self.basepath + self.rest['channels'] % categoryID
        xml = self._postRequest (url = url)
        
        channels = []

        if(not xml == ""):
            doc = parseString(xml)
                 
            for node in doc.getElementsByTagName("ClientChannelInfoExtended"):
                channels.append({
                            'ChannelID': self.getXmlValue (node, 'ChannelID'),
                            'SortOrder': self.getXmlValue (node, 'SortOrder'),
                            'Title': self.getXmlValue (node, 'Title'),
                            'Description': self.getXmlValue (node, 'Description'),
                            'Logo': self.getXmlValue (node, 'ImageUrl'),
                            'action':'channel',
                            'folder':'true'
                        })
        return channels

    def getPrograms(self, channelID):
        url = self.basepath + self.rest['programs'] % channelID
        xml = self._postRequest (url = url)
        
        programs = []

        if(not xml == ""):
            doc = parseString(xml)
                 
            for node in doc.getElementsByTagName("ProgramInfo"):
                programs.append({
                            'ChannelID': self.getXmlValue (node, 'ChannelID'),
                            'Description': self.getXmlValue (node, 'Description'),
                            'Duration': self.getXmlValue (node, 'Duration'),
                            'EndTime': self.getXmlValue (node, 'EndTime'),
                            'ProgramID': self.getXmlValue (node, 'ProgramID'),
                            'RunTime': self.getXmlValue (node, 'RunTime'),
                            'StartTime': self.getXmlValue (node, 'StartTime'),
                            'Title': self.getXmlValue (node, 'Title'),
#                            'Logo': self.getXmlValue (node, 'ImageUrl'),
                            'action':'program',
                            'folder':'true'
                        })
        return programs

    def getProgramsByDate(self, channelID, date):
#        print "Date: " + date.strftime('%Y-%m-%d')
        url = self.basepath + self.rest['guide'] % (channelID, date.strftime('%Y-%m-%d'))
        xml = self._postRequest (url = url)
        
        programs = []

        if(not xml == ""):
            doc = parseString(xml)
                 
            for node in doc.getElementsByTagName("ProgramInfo"):
                programs.append({
                            'ChannelID': self.getXmlValue (node, 'ChannelID'),
                            'Description': self.getXmlValue (node, 'Description'),
                            'Duration': self.getXmlValue (node, 'Duration'),
                            'EndTime': self.getXmlValue (node, 'EndTime'),
                            'ProgramID': self.getXmlValue (node, 'ProgramID'),
                            'RunTime': self.getXmlValue (node, 'RunTime'),
                            'StartTime': self.getXmlValue (node, 'StartTime'),
                            'Title': self.getXmlValue (node, 'Title'),
#                            'Logo': self.getXmlValue (node, 'ImageUrl'),
                            'action':'program',
                            'folder':'true'
                        })
        return programs

    def getMediaUrl(self, channelID = ''):
        retval = ''
        url = self.basepath + self.rest['play'] % channelID
        xml = self._postRequest (url)

        if(not xml == ""):
            doc = parseString(xml)
            urlxml = doc.documentElement.firstChild
            retval = 'http://' + urlxml.nodeValue
        return retval

    def getXmlValue (self, root, tag):
        retval = ''
        for child in root.childNodes:
            if(child.tagName == tag):
                try:
                    tagvalue = child.firstChild
                    retval =  tagvalue.nodeValue
                except:
                    retval = ""
        return retval



