import xbmc
import grtvSettings 
from strings import *

class homeUpdater:
    def __init__(self, settings = None):
        if(settings):
            self._settings = settings
        else:
            self._settings = grtvSettings.grtvSettings()
        
    def OverwriteHome(self):
        i = 1
        xbmc.executebuiltin("Skin.SetString(menu%s.label, %s)" %( i, strings(GUIDE)))
        xbmc.executebuiltin("Skin.SetString(menu%s.bg, )" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.master, addon%s)" %(i,i))
        xbmc.executebuiltin("Skin.SetString(menu%s.subid, addon%s)" %(i,i))
        xbmc.executebuiltin("Skin.SetString(menu%s.action, RunScript(script.grtv.stb))" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.visible, )" %i)
        i = i+1
        
        xbmc.executebuiltin("Skin.SetString(menu%s.label, %s)" %( i, strings(GUIDEHD)))
        xbmc.executebuiltin("Skin.SetString(menu%s.bg, )" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.master, addon%s)" %(i,i))
        xbmc.executebuiltin("Skin.SetString(menu%s.subid, addon%s)" %(i,i))
        xbmc.executebuiltin("Skin.SetString(menu%s.action, RunScript(script.tvguide.hellenic))" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.visible, )" %i)
        i = i+1
        
        xbmc.executebuiltin("Skin.SetString(menu%s.label, %s)" %( i, strings(ONDEMAND)))
        xbmc.executebuiltin("Skin.SetString(menu%s.bg, )" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.master, addon%s)" %(i,i))
        xbmc.executebuiltin("Skin.SetString(menu%s.subid, addon%s)" %(i,i))
        xbmc.executebuiltin("Skin.SetString(menu%s.action, RunScript(script.grtv.ondemand))" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.visible, )" %i)
        i = i+1
        
#        xbmc.executebuiltin("Skin.SetString(menu%s.label, %s)" %( i, strings(FOOTBALL)))
#        xbmc.executebuiltin("Skin.SetString(menu%s.bg, )" %i)
#        xbmc.executebuiltin("Skin.SetString(menu%s.master, addon%s)" %(i,i))
#        xbmc.executebuiltin("Skin.SetString(menu%s.subid, addon%s)" %(i,i))
#        xbmc.executebuiltin("Skin.SetString(menu%s.action, RunAddon(plugin.video.football.today))" %i)
#        xbmc.executebuiltin("Skin.SetString(menu%s.visible, )" %i)
#        i = i+1
        
        xbmc.executebuiltin("Skin.SetString(menu%s.label, %s)" %( i, strings(MUSIC)))
        xbmc.executebuiltin("Skin.SetString(menu%s.bg, )" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.master, music)" %(i))
        xbmc.executebuiltin("Skin.SetString(menu%s.subid, music)" %(i))
#        xbmc.executebuiltin("Skin.SetString(menu%s.master, addon%s)" %(i,i))
#        xbmc.executebuiltin("Skin.SetString(menu%s.subid, addon%s)" %(i,i))
        xbmc.executebuiltin("Skin.SetString(menu%s.action, RunAddon(plugin.audio.hellenic.radio.grtv))" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.visible, )" %i)
        i = i+1
        
        # MQ5 Weather Home
        xbmc.executebuiltin("Skin.SetString(menu%s.label, %s)" %( i, strings(WEATHER)))
        xbmc.executebuiltin("Skin.SetString(menu%s.bg, )")
        xbmc.executebuiltin("Skin.SetString(menu%s.master, weather)" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.subid, weather)" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.action, ActivateWindow(weather))" %i)
        if(self._settings.showWeather() == "false"):
            xbmc.executebuiltin("Skin.SetString(menu%s.visible, off)" %i)
            #Other skins Weather Home
            xbmc.executebuiltin("Skin.SetString(grtvmenuweather.visible, off)")
        else:
            xbmc.executebuiltin("Skin.SetString(menu%s.visible, )" %i)
            xbmc.executebuiltin("Skin.SetString(grtvmenuweather.visible, )")
        i = i+1
        
        xbmc.executebuiltin("Skin.SetString(menu%s.label, %s)" %( i, strings(CHANNELS)))
        xbmc.executebuiltin("Skin.SetString(menu%s.bg, )" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.master, movies)" %(i))
        xbmc.executebuiltin("Skin.SetString(menu%s.subid, movies)" %(i))
        xbmc.executebuiltin("Skin.SetString(menu%s.action, RunAddon(plugin.video.grtv))" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.visible, )" %i)
        i = i+1
        
        
        xbmc.executebuiltin("Skin.SetString(menu%s.label, %s)" %( i, strings(MYGRTV)))
        xbmc.executebuiltin("Skin.SetString(menu%s.bg, )" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.master, addon%s)" %(i,i))
        xbmc.executebuiltin("Skin.SetString(menu%s.subid, addon%s)" %(i,i))
        xbmc.executebuiltin("Skin.SetString(menu%s.action, RunPlugin(plugin://plugin.video.mygrtv?action=settings))" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.visible, )" %i)
        i = i+1

        xbmc.executebuiltin("Skin.SetString(menu%s.label, ADVANCED SETTINGS)" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.bg, )" %i)
        xbmc.executebuiltin("Skin.SetString(menu%s.master, settings)" %(i))
        xbmc.executebuiltin("Skin.SetString(menu%s.subid, settings)" %(i))
        xbmc.executebuiltin("Skin.SetString(menu%s.action, ActivateWindow(settings))" %i)
        if(self._settings.showAdvancedSettings() == "false"):
            xbmc.executebuiltin("Skin.SetString(menu%s.visible, off)" %i)
            #Other skins settings Home
            xbmc.executebuiltin("Skin.SetString(grtvmenusystem.visible, off)")
            xbmc.executebuiltin("Skin.SetBool(HomeMenuNoAdvancedSettingsButton)")
        else:
            xbmc.executebuiltin("Skin.SetString(menu%s.visible, )" %i)
            xbmc.executebuiltin("Skin.SetString(grtvmenusystem.visible, )")
            xbmc.executebuiltin("Skin.Reset(HomeMenuNoAdvancedSettingsButton)")
            
        i = i+1


 
        for x in range(i, 20):
            xbmc.executebuiltin("Skin.SetString(menu%s.label, NOT SET)" % x )
            xbmc.executebuiltin("Skin.SetString(menu%s.bg, )" % x )
            xbmc.executebuiltin("Skin.SetString(menu%s.master, addon%s)" % (x, x) )
            xbmc.executebuiltin("Skin.SetString(menu%s.subid, addon%s)" % (x, x) )
            xbmc.executebuiltin("Skin.SetString(menu%s.action, )" % x )
            xbmc.executebuiltin("Skin.SetString(menu%s.visible, off)" % x )
#    print "menu%spreset1.label: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.label)" % x )
#    print "menu%spreset1.bg: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.bg)" % x )
#    print "menu%spreset1.master: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.master)" % x )
#    print "menu%spreset1.subid: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.subid)" % x )
#    print "menu%spreset1.action: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.action)" % x )
#    print "menu%spreset1.condition: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.condition)" % x )
