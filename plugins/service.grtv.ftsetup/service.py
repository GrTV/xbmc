# -*- coding: utf-8 -*-

'''
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import json
from os.path import join
import xbmc, xbmcvfs, xbmcaddon

addon = xbmcaddon.Addon()
transpath = xbmc.translatePath

addon_id = 'service.grtv.ftsetup'
jsonrpc = xbmc.executeJSONRPC


def json_rpc(command):

    try:
        base_string = basestring
    except BaseException:
        base_string = str

    if not isinstance(command, base_string):
        command = json.dumps(command)
    response = jsonrpc(command)

    return json.loads(response)


def weather_set_up():

    addon_settings = '''<settings>
    <setting id="Location1" value="Toronto (CA)" />
    <setting id="Location1id" value="4118" />
    <setting id="Location2" value="" />
    <setting id="Location2id" value="" />
    <setting id="Location3" value="" />
    <setting id="Location3id" value="" />
    <setting id="Location4" value="" />
    <setting id="Location4id" value="" />
    <setting id="Location5" value="" />
    <setting id="Location5id" value="" />
</settings>
'''

    location = transpath('special://profile/addon_data/weather.yahoo')
    if not xbmcvfs.exists(location):
        xbmcvfs.mkdirs(location)

    with open(join(location, 'settings.xml'), mode='w') as f:
        f.write(addon_settings)

    set_a_setting('weather.addon', 'weather.yahoo')
    xbmc.executebuiltin('Weather.Refresh')


def rss_setup():

    filename = xbmc.translatePath(join('special://profile', 'RssFeeds.xml'))
    f = open(filename, 'r+')
    f.seek(0)
    f.writelines('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n')
    f.writelines('<rssfeeds>\n')
    f.writelines('  <set id="1">\n')
    f.writelines('    <feed updateinterval="20">http://www.greeknamedays.gr/tools/eortologiorssfeed/index.php?langid=gr</feed>\n')
    f.writelines('    <feed updateinterval="20">http://feeds.feedburner.com/skai/yinm</feed>\n')
    f.writelines('    <feed updateinterval="20">http://feeds.feedburner.com/skai/TfmK</feed>\n')
    f.writelines('    <feed updateinterval="20">http://www.enet.gr/rss?i=news.el.article</feed>\n')
    f.writelines('  </set>\n')
    f.writelines('</rssfeeds>\n')
    f.truncate()
    f.close()
    xbmc.executebuiltin("RefreshRSS()")


def set_a_setting(setting, value):

    json_cmd = {"jsonrpc": "2.0", "method": "Settings.SetSettingValue", "params": {"setting": setting, "value": value}, "id": 1}

    json_rpc(json_cmd)


def set_skin_setting(setting_id, state='true'):

    return xbmc.executebuiltin('Skin.SetBool({0},{1})'.format(setting_id, state))


def set_skin_string_setting(setting_id, string):

    return xbmc.executebuiltin('Skin.SetString({0},{1})'.format(setting_id, string))


def shortcuts():

    xbmc.sleep(1000)
    set_skin_setting('HomeMenuNoPicturesButton', 'true')
    set_skin_string_setting('HomeProgramButton1', 'script.grtv.stb')
    set_skin_string_setting('HomeVideosButton1', 'plugin.video.grtv')
    set_skin_string_setting('HomeMusicButton1', 'plugin.audio.eradio.gr')


def prep():

    xbmc.executebuiltin('ActivateWindow(busydialog)')
    set_a_setting('locale.keyboardlayouts', ['English QWERTY', 'Greek QWERTY'])
    weather_set_up()
    rss_setup()
    xbmc.executebuiltin('Dialog.Close(busydialog)')


def additional():

    if 'kodi' in xbmc.getInfoLabel('System.FriendlyName').lower():

        set_a_setting('locale.country', 'Canada') #
        set_a_setting('screensaver.mode', 'screensaver.atv4')
        set_a_setting('videoplayer.stretch43', '4')


if __name__ == '__main__':

    try:

        shortcuts()

        first_time_loc = transpath(join('special://profile', 'addon_data', addon_id))

        if xbmcvfs.exists(join(first_time_loc, 'DO_NOT_DELETE')):
            raise BaseException
        elif not xbmcvfs.exists(first_time_loc):
            xbmcvfs.mkdirs(first_time_loc)
            open(join(first_time_loc, 'DO_NOT_DELETE'), 'a').close()

            prep()
            additional()

        else:
            raise BaseException

    except BaseException:
        pass
