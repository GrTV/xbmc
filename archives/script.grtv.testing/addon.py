#
#      Copyright (C) 2013 Team GrTV
#      http://www.grtv.mobi
#
#  This Program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2, or (at your option)
#  any later version.
#
#  This Program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this Program; see the file LICENSE.txt.  If not, write to
#  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#  http://www.gnu.org/copyleft/gpl.html
#

import sys
import xbmc
import xbmcgui
import xbmcplugin

import grtvSettings
settings = grtvSettings.grtvSettings()

# xbmc hooks

for x in range(1, 20):
    print "menu%spreset1.label: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.label)" % x )
    print "menu%spreset1.bg: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.bg)" % x )
    print "menu%spreset1.master: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.master)" % x )
    print "menu%spreset1.subid: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.subid)" % x )
    print "menu%spreset1.action: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.action)" % x )
    print "menu%spreset1.condition: " % x + xbmc.getInfoLabel("Skin.String(menu%spreset1.condition)" % x )


for x in range(1, 20):
    print "menu%s.label: " % x + xbmc.getInfoLabel("Skin.String(menu%s.label)" % x )
    print "menu%s.bg: " % x + xbmc.getInfoLabel("Skin.String(menu%s.bg)" % x )
    print "menu%s.master: " % x + xbmc.getInfoLabel("Skin.String(menu%s.master)" % x )
    print "menu%s.subid: " % x + xbmc.getInfoLabel("Skin.String(menu%s.subid)" % x )
    print "menu%s.action: " % x + xbmc.getInfoLabel("Skin.String(menu%s.action)" % x )
    print "menu%s.visible: " % x + xbmc.getInfoLabel("Skin.String(menu%s.visible)" % x )

#print xbmc.getInfoLabel('preset1.label')
xbmc.executebuiltin("Skin.SetString(preset1.label, GRTV TEST)")
