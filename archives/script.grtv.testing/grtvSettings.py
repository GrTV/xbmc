import xbmcaddon

class grtvSettings:
    def __init__(self):
        self._settingsAddon = xbmcaddon.Addon(id='plugin.video.mygrtv')
#        self._settings = self._settingsAddon.settings


    def userName(self):
        return self._settingsAddon.getSetting("username")

    def userPassword(self):
        return self._settingsAddon.getSetting("password")

    def applicationID(self):
        return self._settingsAddon.getSetting("appId")

    def showAdvancedSettings(self):
        return self._settingsAddon.getSetting("system")

    def showWeather(self):
        return self._settingsAddon.getSetting("weather")

    def AutoStart(self):
        return self._settingsAddon.getSetting("channelsOnStartup") == "true"

    def _getUserCredentials(self):
        xml = '<PlayerLogin xmlns="http://schemas.datacontract.org/2004/07/DnnIptv.PlayerService.Common.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ApplicationID>' + self.applicationID() + '</ApplicationID><ApplicationName>XBMC Plugin</ApplicationName><ApplicationVersion>1.0.0</ApplicationVersion><DeviceType>XBMC</DeviceType><Login>' + self.userName() + '</Login><Password>' + self.userPassword() + '</Password></PlayerLogin>'
        return xml
        
